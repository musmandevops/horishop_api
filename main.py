from flask import Flask
from flask import render_template
from waitress import serve
from flaskext.mysql import MySQL
from flask_cors import CORS
import json
import os

import random

app = Flask(__name__,template_folder='template')
CORS(app)
mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = os.environ.get('DB_USER')
app.config['MYSQL_DATABASE_PASSWORD'] = os.environ.get('DB_PASSWORD')
app.config['MYSQL_DATABASE_DB'] = os.environ.get('DB_NAME')
app.config['MYSQL_DATABASE_HOST'] = os.environ.get('DB_HOST')
mysql.init_app(app)


@app.route('/hello/<name>')
def hello_name(name):
    sentences = ["You're Exceptional!","You're Special!","You're A Great Example For Others!","You Made The Difference!", \
                    "You're Unique!","You're Inspiring!","You're Getting Better!"]
    return """Hello %s, %s""" % (name.capitalize(), str(sentences[random.randint(0,len(sentences)-1)]))

@app.route("/hello")
def debug():
    return 'Hello World!'

@app.route("/")
def home():
    return render_template('home.html')


@app.route('/users')
def users():
    query = "select id,firstname,lastname,email,reg_date from users"
    try:
        conn = mysql.connect()
        cursor =conn.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
    except Exception as e:
        return "Execption in database: " + str(e)
    response = {}
    for row in data:
        response[row[0]] = {"first_name":str(row[1]),"last_name":str(row[2]),"email":str(row[3]),"reg_date":str(row[4])}

    if not response:
        response = {"message":"record not found"}
    response = json.dumps(response)
    print(type(response))
    return response

@app.route('/user/<name>')
def get_user(name):
    query = "select id,firstname,lastname,email,reg_date from users where firstname= '" + str(name) + "'"
    try:
        conn = mysql.connect()
        cursor =conn.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
    except Exception as e:
        return "Execption in database: " + str(e)
    response = {}
    for row in data:
        response[row[0]] = {"first_name":str(row[1]),"last_name":str(row[2]),"email":str(row[3]),"reg_date":str(row[4])}

    if not response:
        response = {"message":"record not found"}
    response = json.dumps(response)
    return response


if __name__ == '__main__':
    if os.environ.get('APP_ENV') == "production":
        serve(app, host="0.0.0.0", port=5000)
    else:
        app.run(host='0.0.0.0', port=5000)
